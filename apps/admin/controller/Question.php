<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
//use think\Paginator;
use app\admin\model\Questions;
use app\admin\model\Answers;
use app\admin\controller\Siku;
class Question extends Siku
{
    
    public function index()
    {
        
        //echo ROOT_PATH."public/static/ueditor/php/config.json";
        //echo strip_tags('<b>fff </b>','<b>');
        //echo request()->domain();// 框架应用根目录
        //$qdata=Questions::paginate(1);//直接全部带分页
        //$list = new Questions;
        //$qdata =$list->with('getanswers')->select();
        //$qdata=$list->relation('getanswers')->select();
        //dump($qdata->toArray());
        //return json($qdata);

         //$this->assign('qdata',$qdata);
         //$this->assign('page',$qdata->render());
        $exams=db('exams')->where('subject', session('subject'))->order('eid', 'asc')->select()->toArray();
        //dump($exams);
        $this->assign('exams',$exams);
         return $this->fetch();
    }
    
    public function qdata_tiku()
    {
        $limit=request()->param('limit');
        $page=request()->param('page');
        $map=[];
        $qtype=request()->param('qtypetiku');
        !empty($qtype)?$map['qtype']=$qtype:'';
        // $qsubject=request()->param('subjecttiku');//where('qid','in',$qids)
        // !empty($qsubject)?$map['qsubject']=$qsubject:'';
        $qids=request()->param('examhistroy');
        !empty($qids)?$map['qid']=['in',$qids]:'';
        //dump($map);
        //$map = array_diff($map, array(null,'null','',' '));
        //$count=$tdata->where($map)->count();
         $list = new Questions;
         $with['getanswers']=function($query){$query->order(['aid'=>'asc']);};
        $qdata =$list->page($page,$limit)->where($map)->where('qsubject',session('subject'))->with($with)->with('getteacher')->select();
        $count=$list->where($map)->where('qsubject',session('subject'))->count();
        //dump($qdata);qsubject=session('subject')
    
        // $data=array('qdata' => $1data);
         return myjson(0,'',$count,$qdata);
    }
    public function qdata()
    {
        $limit=request()->param('limit');
        $page=request()->param('page');
        $map['qtype']=request()->param('qtype');
        //$map['qsubject']=request()->param('subject');
        $map = array_diff($map, array(null,'null','',' '));
        //$count=$tdata->where($map)->count();
         $list = new Questions;
         $with['getanswers']=function($query){$query->order(['aid'=>'asc']);};
        $qdata =$list->page($page,$limit)->where($map)->where('qstatus',0)->where('tid',session('uid'))->with($with)->with('getteacher')->order('qid','asc')->select();
        $count=$list->where($map)->where('qstatus',0)->where('tid',session('uid'))->count();
        //dump($qdata);
    
        // $data=array('qdata' => $1data);
         return myjson(0,'',$count,$qdata);
    }
    public function add()
    {
        
        return $this->fetch();
        
    }
    public function do_add(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            $aa=answer_str_img($formdata['answer']);
            $answer=explode('<br>', $aa);
            //dump($answers);
            $answer_num=strlen(trim($formdata['right']));
            $answer_num > 1 ? $qtype=2 : $qtype=1;
            //echo $qtype;
            //dump($answers);
            $qdata  = new Questions;
            $qdata->qtitle=question_str_img($formdata['question']);
            $qdata->qtype=$qtype;
            $qdata->rightselect=trim($formdata['right']);
            $qdata->tid=session('uid');
            $qdata->qsubject=session('subject');
            $qdata->save();
            //获取自增ID
            $qid= $qdata->qid;
            
            //dump($answers);
            if ($qid) {
                for ($i=0; $i < count($answer); $i++) {
                    $answers[$i]['qid']=$qid;
                    $answers[$i]['answercontent']=$answer[$i];
                    //$answers[$i]['isright']=intval($formdata['isright'][$i]); 
                    if (in_array(substr($answer[$i], 0,1),ch2arr($formdata['right']),true)) {
                        $answers[$i]['isright']=1;
                    } else {
                        $answers[$i]['isright']=0;
                    }
                }
                $adata  = new Answers;
                $res=$adata->saveAll($answers); 
                if ($res) {
                      $re=1;//where left(name, 1)='H';
                      $rightanswer=db('answers') ->where('qid',$qid)->where('isright',1)->order('aid','asc')->select()->toArray();
                      $rightanswers=implode(",",i_array_column($rightanswer,'aid'));
                      db('questions')->where('qid',$qid)->setField('rightanswer', $rightanswers);
                  }else{
                        $re='系统错误，请刷新重试！';
                  } 

            }
           
            
            return $re;
        }else{
            return '非法操作！';
        }
    }
    //修改添加试题方法的备份
    public function do_add_back(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            $aa=answer_str_img($formdata['answer']);
            dump($aa);
            dump(explode('<br>', $aa));
           if(!array_key_exists('isright',$formdata)){
                $re= '没有指定正确答案';//没选答案
           }else{
             //dump($formdata);
                $qdata  = new Questions;
                $qdata->qtitle=question_str_img($formdata['question']);
                $qdata->qtype=$formdata['qtype'];
                $qdata->tid=session('uid');
                $qdata->qsubject=session('subject');
                $qdata->save();
                // 获取自增ID
                $qid= $qdata->qid;
                if ($qid) {
                    $answers=array();
                    for ($i=0; $i < count($formdata['answer']); $i++) { 
                       //echo $i;
                            
                        if (array_key_exists($i,$formdata['isright'])) {//循环正确答案的序号
                            $answers[$i]['qid']=$qid;
                            $answers[$i]['answercontent']=question_str_img($formdata['answer'][$i]);
                            $answers[$i]['isright']=intval($formdata['isright'][$i]);
                        } else {
                            $answers[$i]['qid']=$qid;
                            $answers[$i]['answercontent']=question_str_img($formdata['answer'][$i]);
                            $answers[$i]['isright']=0;
                        }   
                    }
                    $adata  = new Answers;
                    $res=$adata->saveAll($answers); 
                    if ($res) {
                          $re=1;
                          $rightanswer=db('answers') ->where('qid',$qid)->where('isright',1)->select()->toArray();
                          $rightanswers=implode(",",i_array_column($rightanswer,'aid'));
                          db('questions')->where('qid',$qid)->setField('rightanswer', $rightanswers);
                      }else{
                            $re='系统错误，请刷新重试！';
                      } 

                }
           }
            
            return $re;
        }else{
            return '非法操作！';
        }
    }


    public function select(){
        $id=request()->param('id');
        //echo $id;$with['getanswers']=function($query){$query->order(['aid'=>'asc']);};
       // $qdata =$list->page($page,$limit)->where($map)->where('qsubject',session('subject'))->with($with)->with('getteacher')->select();
        $q= new Questions;
        $with['getanswers']=function($query){$query->order(['aid'=>'asc']);};
       $qdata =$q->where('qid',$id)->with($with)->find();
        //dump($qdata->toArray());
        $this->assign('qdata',$qdata);
        return $this->fetch();
    }
    public function select_edit(){
        //echo $id;
        if (request()->isPost()) {
            $formdata = request()->post();
           if(!array_key_exists('isright',$formdata)){
                $re= '没有指定正确答案';//没选答案
           }else{
             //dump($formdata);
             $rightanswer=implode(',',array_keys($formdata['isright']));$rightselect='';
             foreach ($formdata['isright'] as $rid => $v) {
                //用mb_substr防止中文乱码
                 $rightselect.=mb_substr(answer_str_img($formdata['answer'][$rid]),0,1);
             }
             //echo $rightselect;
             count($formdata['isright']) > 1 ? $qtype=2 : $qtype=1;
                $qid=$formdata['qid'];
                $qdata  = Questions::get($qid);
                $qdata->qtitle=question_str_img($formdata['question']);//自写公共函数
                $qdata->qtype=$qtype;
                $qdata->tid=session('uid');
                $qdata->qsubject=session('subject');
                $qdata->rightselect=$rightselect;
                $qdata->rightanswer=$rightanswer;
                $res=$qdata->save();
                $answers=array();$i=0;
                foreach ($formdata['answer'] as $key => $value) {
                    $answers[$i]['aid']=$key;
                    $answers[$i]['answercontent']=answer_str_img($value);
                    $answers[$i]['qid']=$qid;
                    if (in_array($key, array_keys($formdata['isright']),true)) {
                        $answers[$i]['isright']=1;
                    } else {
                        $answers[$i]['isright']=0;
                    }
                    
                    $i++;
                }
                //dump($answers);
                $adata  = new Answers;
                $resa=$adata->isUpdate(true)->saveAll($answers); 
                if ($resa) {
                      $re=1;
                  }else{
                        $re='系统错误，请刷新重试！';
                  }  
            }
            
            return $re;
        }else{
            return '非法操作！';
        }
    }
    public function title_add(){
        $titledata=request()->post();
        //dump($subdata);
        $qdata  = new Questions;
        $qdata->qtitle=$titledata['qtitle'];
        $qdata->qtype=$titledata['qtype'];
        $qdata->tid=session('uid');
        $qdata->qsubject=session('subject');
        $re=$qdata->save();
        return $re;
    }
    public function title(){
        $qid=request()->param('id');
        //echo $qid;
        $qdata=db('questions')->find($qid);
        //dump($qdata);
        $this->assign('qdata',$qdata);
        return $this->fetch();
    }
    public function title_edit(){
        $subdata=request()->post();
        //dump($subdata);
        $re=db('questions')->where('qid',$subdata['qid'])->setField('qtitle', $subdata['qtitle']);
        return $re;
    }
    public function del(){
        $qid=request()->param('id');
        $rea=db('answers')->where('qid',$qid)->delete();
        $re=db('questions')->delete($qid);
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        return $res;
    }
    public function tiankong_add(){
        if (request()->isPost()) {
            $formdata = request()->post();
            $qdata  = new Questions;
            $qdata->qtitle=question_str_img($formdata['question']);
            $qdata->qtype=3;
            $qdata->rightselect=trim($formdata['right']);
            $qdata->tid=session('uid');
            $qdata->qsubject=session('subject');
            $re=$qdata->save();
            //获取自增ID
            return $re;
        }else{
            return '非法操作！';
        }
    }
    public function tiankong(){
        $qid=request()->param('id');
        //echo $qid;
        $qdata=db('questions')->find($qid);
        //dump($qdata);
        $this->assign('qdata',$qdata);
        return $this->fetch();
    }
    public function tiankong_edit(){
        $formdata = request()->post();
        $qid=$formdata['qid'];
        $qdata  = Questions::get($qid);
        $qdata->qtitle=question_str_img($formdata['tiankong']);//自写公共函数
        $qdata->rightselect=trim($formdata['right']);
        $res=$qdata->save();
        if ($res) {
            return 1;
        } else {
            return '修改失败！';
        }
        
    }


   
    
     
    

























    
    
    
   
    public function showimg(){
        $data=request()->param('data');
        return $data['id'];
    }
    public function other()
    {
    	return '其他功能按需开发！';
    }

    public function def(){
        $tdata=new Tdata;
        $tol=$tdata->count();
        $num=$tdata->distinct(true)->field('listener')->select();
        $kemu=db('tdata')->field('subject as name,count(id) as value')->group('subject')->select();
        $grade=db('tdata')->field('grade as name,count(id) as value')->group('grade')->select();
        //dump(i_array_column($kemu, 'name'));
        $this->assign('grade',$grade);
        $this->assign('kemu',$kemu);
        $this->assign('tol',$tol);
        $this->assign('num',count($num));
    	return $this->fetch();
    }
    // public function tubiao(){
    //     return $this->fetch();
    // }
    public function tubiao(){
        //echo request()->post('listener');
        $tname=request()->post('listener');
        $starttime=request()->post('starttime');
        $endtime=request()->post('endtime');
        if (!empty($tname) && !empty($starttime) && !empty($endtime)) {
            $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (!empty($tname) && empty($starttime) && empty($endtime)) {
            $listbar=db('tdata')->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (empty($tname) && !empty($starttime) && !empty($endtime)) {
           $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->field('listener as name,count(id) as value')->group('listener')->select();
        }else{
            $listbar=db('tdata')->field('listener as name,count(id) as value')->group('listener')->select();
        }
        $this->assign('listbar',$listbar);
         return $this->fetch();

    }
}
